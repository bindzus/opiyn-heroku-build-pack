# opiyn-heroku-build-pack

Use multiple buildpacks on your app

## Usage

    $ heroku buildpacks:set https://bindzus@bitbucket.org/bindzus/opiyn-heroku-build-pack.git

    $ cat .buildpacks
    https://github.com/heroku/heroku-buildpack-nodejs.git#0198c71daa8
    https://github.com/heroku/heroku-buildpack-ruby.git#v86